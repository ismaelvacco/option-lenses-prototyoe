

export default {
  setStep({commit}, step) {
    // console.log(step);
    commit('setStepState', step);
  },

  setOption({state, commit}, option) {
    const optionIndex = state.actualOption.findIndex(optionState => {
      return optionState.step === state.actualStep
    });
    if (optionIndex > -1) {
      commit('deleteOptionState', optionIndex);
    }
    commit('setOptionState', {
      option: option.option,
      optionKey: option.optionKey,
    });
  },

  setDependentsIds({state, commit}, dependentIds) {
    const dependentIndex = state.dependentIds.findIndex(dependent => {
      return dependent.step === state.actualStep
    });
    if (dependentIndex > -1) {
      commit('deleteDependentsIdsState', dependentIndex);
    }
    commit('setDependentsIdsState', {
      step: state.actualStep,
      dependentIds: dependentIds,
    });
  },

  validate({state, commit}) {
    if (state.actualOption.length === 3) {
      commit('doValid');
      return;
    }

    commit('doInvalid');
  }
}
