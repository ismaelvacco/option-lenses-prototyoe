
export const mutations = {
  setStepState(state, step) {
    state.actualStep = step;
  },

  setOptionState(state, option) {
    state.actualOption.push({
      step: state.actualStep,
      option: option.option,
      optionKey: option.optionKey,
    });
  },

  deleteOptionState(state, index) {
    state.actualOption.splice(index, 1);
  },

  setDependentsIdsState(state, dependentIds) {
    state.dependentIds.push(dependentIds);
  },

  deleteDependentsIdsState(state, index) {
    state.dependentIds.splice(index, 1);
  },

  doValid(state) {
    state.isValid = true
  },

  doInvalid(state) {
    state.isValid = false
  },

}
